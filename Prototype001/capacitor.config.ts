import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'Prototype001',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
